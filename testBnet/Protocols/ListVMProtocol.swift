//
//  ListVMProtocol.swift
//  testBnet
//
//  Created by MAC OS on 27/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation
import RxSwift


protocol ListVMProtocol {
    var items : Observable<[EntryRM]> { get }
    var selectedItem : Observable<EntryRM?> { get }
    var refreshing : Observable<Bool> { get }
    var errorMessage : Observable<MessageLM> { get }
    
    func update()
    func selectItem(index: Int)
    func deselectItem()
}
