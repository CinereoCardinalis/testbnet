//
//  NewEntryVM.swift
//  testBnet
//
//  Created by MAC OS on 26/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation
import RxSwift


//viewModel для работы с созданием новой "записи"
class NewEntryVM {
    
    private let disposeBag = DisposeBag()
    
    //ответ на запрос создания "записи"
    let creation = PublishSubject<Bool>()
    
    //запрос на создание новой "записи"
    func createEntry(text: String) {
        struct Simple : Decodable {
            let id : String
        }
        
        BNetAPI.addEntry(text: text)
            .map({ try JSONDecoder().decode(DefaultRM<Simple>.self, from: $0.1) })
            .map({ $0.data?.id == nil ? false : true })
            .subscribe(self.creation)
            .disposed(by: self.disposeBag)
    }
}
