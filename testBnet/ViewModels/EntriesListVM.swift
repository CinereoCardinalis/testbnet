//
//  EntriesListVM.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation
import RxSwift

//viewModel для работы с доступными "записями"
class EntriesListVM: ListVMProtocol {
    
    
    var items: Observable<[EntryRM]> {
        return entries.asObservable()
    }
    
    var selectedItem: Observable<EntryRM?> {
        return selected.asObservable()
    }
    
    var refreshing: Observable<Bool> {
        return state.asObservable()
    }
    
    var errorMessage: Observable<MessageLM> {
        return errors.asObservable()
    }
    
    
    private let disposeBag = DisposeBag()
    
    //Доступные "записи"
    let entries = BehaviorSubject<[EntryRM]>(value: [])
    //Выбранная "запись"
    let selected = BehaviorSubject<EntryRM?>(value: nil)
    //Сигнал о статусе запроса
    let state = PublishSubject<Bool>()
    //Рассылка сообщений об ошибках
    let errors = PublishSubject<MessageLM>()
    
    init() {
        self.entries.subscribe ({ [weak self] _ in self?.state.onNext(false) })
            .disposed(by: self.disposeBag)
        
        self.errorMessage.subscribe({ [weak self] _ in self?.state.onNext(false) })
            .disposed(by: self.disposeBag)
    }
    
    //Запрос на получение существующих "записей"
    func update() {
        state.onNext(true)
        
        //Получение записей
        let entries = BNetAPI.getEntries()
            .map({
                try JSONDecoder().decode(DefaultRM<[[EntryRM]]>.self, from: $0.1) })
            .map({ (defaultModel) -> [[EntryRM]] in
                if let error = defaultModel.error {
                    throw BNetAPIError.error(error)
                } else if let data = defaultModel.data {
                    return data
                }
                throw BNetAPIError.invalid
            })
            .map({ $0.flatMap({ $0 }) })
        
        //Отправка данных в entries
        //А ошибок в errorMessage
        entries
            .subscribe({ [weak self] in
                guard let `self` = self else { return }
                switch $0 {
                case .next(let elem):
                    self.entries.onNext(elem)
                case .error(let error):
                    guard let error = error as? BNetAPIError else { return }
                    self.errors.onNext(MessageLM(message: error.message))
                default: break
                }
            })
            .disposed(by: self.disposeBag)
        
    }
    
    //Получение выбранного элемента
    func selectItem(index: Int) {
        do {
            let entry = try entries.value()[index]
            self.selected.onNext(entry)
        } catch {
            self.selected.onError(error)
        }
    }
    
    //Отмена выбора элемента
    func deselectItem() {
        self.selected.onNext(nil)
    }
    
}
