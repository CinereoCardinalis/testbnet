//
//  BnetErrors.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation

//Возможные ошибки связанные с ответами сервера и обработкой этих ответов
enum BNetAPIError: Error {
    //Обертка для серверных ошибок
    case error(String)
    
    //Ошбики связанные с обработкой ответов
    case invalid
    case noSessionKey
    case dateConversion
    case noInternetConnection
    
    //Генерация выводимых сообщений для имеющихся ошибок
    var message: String {
        switch self {
        case .error(let error):
            return error
        case .invalid:
            return "Internal server error"
        case .noSessionKey:
            return "Session key absent"
        case .dateConversion:
            return "Date type is not correct"
        case .noInternetConnection:
            return "Check internet connection"
        }
    }
}
