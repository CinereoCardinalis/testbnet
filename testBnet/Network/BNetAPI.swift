//
//  RequestService.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import RxAlamofire


//Сервис API
struct BNetAPI {
    
    private static let url = URL(string: "https://bnet.i-partner.ru/testAPI/")!
    
    //Универсальные заголовки
    private static let headers: HTTPHeaders = [
        "Content-Type" : "application/x-www-form-urlencoded",
        "token" : "UnYrGcJ-C6-PbUERDZ"
    ]
    
    //Запись нового и чтение существующего ключа сессии
    private static var sessionKey: String? {
        get { return UserDefaults.standard.value(forKey: "SESSION_KEY") as? String }
        set { UserDefaults.standard.set(newValue, forKey: "SESSION_KEY") }
    }
    
    //Проверка наличия ключа сессии
    //Или его получение при отсутствии
    private static func session() -> Observable<Bool> {
        
        if let sessionKey = sessionKey {
            return Observable<Bool>.just(!sessionKey.isEmpty)
        }
        
        let parameters : [String: Any] = [
            "a" : "new_session"
        ]
        
        return defaultRequest(parameters: parameters)
            .responseData()
            .map({ try JSONDecoder().decode(DefaultRM<SessionRM>.self, from: $0.1) })
            .compactMap({ $0.data?.session })
            .map({
                self.sessionKey = $0
                return !($0.isEmpty)
            })
    }
    
    //Запрос существующих "Записей"
    static func getEntries() -> Observable<(HTTPURLResponse, Data)> {
        
        let parameters : [String: Any] = [
            "a" : "get_entries",
            "session" : sessionKey ?? ""
        ]
        
        return sessionRequest(parameters: parameters)
            .responseData()
    }
    
    //Запрос на создание новой "записи"
    static func addEntry(text: String) -> Observable<(HTTPURLResponse, Data)> {
        
        let parameters : [String: Any] = [
            "a" : "add_entry",
            "session" : sessionKey ?? "",
            "body" : text
        ]
        
        return sessionRequest(parameters: parameters)
            .responseData()
    }
    
    //Унифицированный прямой запрос с предварительной проверкой наличия интернета
    static func defaultRequest(parameters: [String: Any]) -> Observable<DataRequest> {
        
        return Observable<Void>.create({ (observer) -> Disposable in
                if let manager = NetworkReachabilityManager(host: "bnet.i-partner.ru"),
                    manager.isReachable {
                    observer.onNext(())
                } else {
                    observer.onError(BNetAPIError.noInternetConnection)
                }
                return Disposables.create()
            })
            .flatMap({ _ in request(.post, url, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers) })
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
    }
    
    //Запрос валидирующий наличие ключа сессии
    static func sessionRequest(parameters: [String: Any]) -> Observable<DataRequest> {
        return self.session()
            .flatMap({ success -> Observable<DataRequest> in
                if success {
                    return defaultRequest(parameters: parameters)
                } else {
                    throw BNetAPIError.noSessionKey
                }
            })
    }
    
}
