//
//  DefaultRM.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation

//Универсальная модель ответа сервера
struct DefaultRM<T : Decodable> : Decodable {
    let status: Int
    let error : String?
    let data : T?
}
