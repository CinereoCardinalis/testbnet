//
//  SessionRM.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation

//модель ответа сервера "ключ сессии"
struct SessionRM : Decodable {
    let session : String
}
