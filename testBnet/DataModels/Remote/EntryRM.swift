//
//  EntryRM.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import Foundation

//модель ответа сервера "запись"
struct EntryRM : Decodable {
    let id : String
    let body : String
    let da : Date
    let dm : Date

    enum CodingKeys: String, CodingKey {
        case id, body, da, dm
    }
    
    //Кастомный декодер для получения дат из строк
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(String.self, forKey: .id)
        self.body = try container.decode(String.self, forKey: .body)
        
        let da = try container.decode(String.self, forKey: .da)
        let dm = try container.decode(String.self, forKey: .dm)
        
        guard let daDouble = Double(da), let dmDouble = Double(dm) else {
            throw BNetAPIError.dateConversion
        }
        
        self.da = Date(timeIntervalSince1970: daDouble)
        self.dm = Date(timeIntervalSince1970: dmDouble)
        
    }
}
