//
//  NewEntryVC.swift
//  testBnet
//
//  Created by MAC OS on 26/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

//viewController для создания новой "записи"
class NewEntryVC: UIViewController {
    
    private let textView = UITextView()
    
    private let disposeBag = DisposeBag()
    
    var viewModel = NewEntryVM()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        bindsViewModel()
    }
    
    //Предподготовка компонентов интерфейса
    private func setupUI() {
        self.navigationItem.title = "New Entry"
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonTapped(_:)))
        
        //Изменение существующей кнопки "назад" на кнопку "отмена"
        //Источник - задание
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButtonTapped(_:)))
        
        self.view.backgroundColor = .white
        self.view.addSubview(textView)
        
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 10)
            .isActive = true
        textView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 10)
            .isActive = true
        textView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 20)
            .isActive = true
        textView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 20)
            .isActive = true
        
        textView.font = .systemFont(ofSize: 16)
    }
    
    //Создание новой "записи"
    @objc func doneButtonTapped(_ sender: Any?) {
        viewModel.createEntry(text: textView.text)
    }
    
    //Вовзращение на предыдущий экран при отмене создания
    @objc func cancelButtonTapped(_ sender: Any?) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //биндинги с viewModel
    private func bindsViewModel() {
        
        //Подписка на результат создания новой "записи"
        //При удаче возращается на предыдущий экран
        viewModel.creation
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] success in
                guard let `self` = self else { return }
                if success {
                    self.navigationController?.popViewController(animated: true)
                }
            })
            .disposed(by: self.disposeBag)
    }

}
