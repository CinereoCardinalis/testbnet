//
//  ViewController.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

//ViewController для отображения "записей" в таблице
class EntriesListVC: UIViewController {
    
    private let tableView = UITableView()
    private let refreshControl = UIRefreshControl()
    private let entryCellName = "EntryTableViewCell"
    
    private let disposeBag = DisposeBag()
    
    let viewModel : ListVMProtocol = EntriesListVM()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bindSelf()
        bindViewModel()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.update()
    }
    

    //Декорирование элементов интерфейса
    private func setupUI() {
        
        self.navigationItem.title = "Entries list"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped(_:)))
        
        self.view.backgroundColor = .white
        self.view.addSubview(self.tableView)
        
        let cellNib = UINib(nibName: "EntryTableViewCell", bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: self.entryCellName)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        tableView.allowsSelection = true
        tableView.allowsMultipleSelection = false
    }
    
    //Переход на экран добавления новой записи
    @objc private func addButtonTapped(_ sender: Any?) {
        let nextVC = NewEntryVC()
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    //Биндинги с viewModel
    private func bindViewModel() {
        
        //Подписка на обновления списка "записей"
        //Заполнение таблицы "записями"
        viewModel.items
            .observeOn(MainScheduler.instance)
            .bind(to: tableView.rx.items(cellIdentifier: entryCellName)) {
                (index, model, cell) in
                guard let cell = cell as? EntryTableViewCell else { return }
                
                //Форматирование даты для вывода на экран
                let formatter = DateFormatter()//ISO8601DateFormatter()
                //formatter.timeZone = TimeZone.init(secondsFromGMT: 10800)
                formatter.dateFormat = "dd.MM.yyyy hh:mm:ss"
                
                cell.createDateLabel.text =
                    "Creation date: \(formatter.string(from: model.da))"
                cell.modifiedDateLabel.text =
                    "Change date: \(formatter.string(from: model.dm))"
                
                //Ограничение в 200 символов для вывода на экран
                // Источник - задание
                cell.entryTextLabel.text = (model.body.count > 200)
                    ? "\(model.body.prefix(197))..."
                    : model.body
                
                //Проверка на идентичность дат создания и изменение
                //В случае равенства изменение не отображать
                //Источник - задание
                cell.modifiedDateLabel.isHidden = model.da == model.dm
                cell.selectionStyle = .none
            }
            .disposed(by: self.disposeBag)
        
        //Подписка на изменение статуса запроса
        //С задержкой для более корректного отображения анмации скрытия
        viewModel.refreshing
            .throttle( .seconds(1), latest: true, scheduler: ConcurrentDispatchQueueScheduler.init(qos: .userInteractive))
            .observeOn(MainScheduler.asyncInstance)
            .bind(to: self.refreshControl.rx.isRefreshing)
            .disposed(by: self.disposeBag)
        
        //Подписка на изменение выбранного элемента
        //Переход на экран полного отображения "записи"
        viewModel.selectedItem
            .compactMap({ $0 })
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] _ in
                guard let `self` = self else { return }
                let nextVC = FullEntryVC(nibName: "FullEntryVC", bundle: nil)
                //Передается эта же вью-модель так как она так же отвечает за выбранную "запись"
                nextVC.viewModel = self.viewModel
                self.navigationController?.pushViewController(nextVC, animated: true)
            })
            .disposed(by: self.disposeBag)
        
        //Подписка на сообщения об ошибках
        //Выводит сообщения обшибках на экран, в том числе и отсутствие соединения с сетью
        viewModel.errorMessage
            .subscribeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] in
                guard let `self` = self else { return }
                let alert = UIAlertController(title: "Error", message: $0.message, preferredStyle: .alert)
                //Кнопка вызывающая обновление
                //Источник - задание
                let action = UIAlertAction(title: "Update", style: .default, handler: { (_) in
                    self.viewModel.update()
                })
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            })
            .disposed(by: self.disposeBag)
    }
    
    //Внутренние биндинги для этого вью-контроллера
    private func bindSelf() {
        
        //Подписка на вызов обновления через свайп
        //Обновляет список "записей"
        refreshControl.rx.controlEvent(.valueChanged)
            .bind(onNext: { [weak self] in
                self?.viewModel.update()
            })
            .disposed(by: self.disposeBag)
        
        //Подписка на событие выбора ячейки пользователем
        //Отправляет во вью-модель индекс этой ячейки
        tableView.rx.itemSelected
            .bind(onNext: { [weak self] in self?.viewModel.selectItem(index: $0.row) })
            .disposed(by: self.disposeBag)
        
        //Подписка на событие отмены выбора ячейки
        //Отсправляет во вью-модель отмену выбора
        tableView.rx.itemDeselected
            .bind(onNext: { [weak self] _ in self?.viewModel.deselectItem() })
            .disposed(by: self.disposeBag)
    }
    
}

