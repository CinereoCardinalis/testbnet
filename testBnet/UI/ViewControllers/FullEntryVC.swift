//
//  FullEntryVC.swift
//  testBnet
//
//  Created by MAC OS on 26/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

//viewController для полного отображения записи
class FullEntryVC: UIViewController {
    
    @IBOutlet private var stackView : UIStackView!
    
    @IBOutlet private var dateCreateLabel : UILabel!
    @IBOutlet private var dateModifiedLabel : UILabel!
    //Было выбрано текстовое поле т.к. оно имеет реализованный скролинг содержимого
    @IBOutlet private var entryText : UITextView!
    
    private let disposeBag = DisposeBag()
    
    //используемая вью-модель
    var viewModel : ListVMProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        bindsViewModel()
    }
    
    
    private func setupUI() {
        self.navigationItem.title = "Entry"
    }
    
    //Биндинги с viewModel
    private func bindsViewModel() {
        guard let viewModel = self.viewModel else { return }
        
        //Подписка на изменение выбранной "записи"
        viewModel.selectedItem
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] (selected) in
                guard let `self` = self, let selected = selected else { return }
                
                //Форматироване даты для вывода на экран
                let formatter = DateFormatter()//ISO8601DateFormatter()
                //formatter.timeZone = TimeZone.init(secondsFromGMT: 10800)
                formatter.dateFormat = "dd.MM.yyyy hh:mm:ss"
                
                self.dateCreateLabel.text =
                    "Creation date: \(formatter.string(from: selected.da))"
                self.dateModifiedLabel.text =
                    "Change date: \(formatter.string(from: selected.dm))"
                
                self.entryText.text = selected.body
            })
            .disposed(by: self.disposeBag)
    }

}
