//
//  EntryTableViewCell.swift
//  testBnet
//
//  Created by MAC OS on 25/09/2019.
//  Copyright © 2019 CinereoCardinalis. All rights reserved.
//

import UIKit

//Ячейка для отобращения записи в таблице
class EntryTableViewCell: UITableViewCell {
    
    @IBOutlet var createDateLabel: UILabel!
    @IBOutlet var modifiedDateLabel: UILabel!
    @IBOutlet var entryTextLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
